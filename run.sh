#!/bin/bash

# Install Docker
sudo apt-get -y update
sudo apt-get -y install     apt-transport-https     ca-certificates     curl     gnupg     lsb-release
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
echo "deb [arch=amd64 signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
sudo apt-get -y update
sudo apt-get -y install docker-ce docker-ce-cli containerd.io
sudo service docker start
sudo usermod -a -G $(whoami)
sudo curl -L \
    https://github.com/docker/compose/releases/latest/download/docker-compose-$(uname -s)-$(uname -m) \
    -o /usr/local/bin/docker-compose
sudo chmod +x /usr/local/bin/docker-compose

# Download selenium docker-nya
sudo docker pull registry.gitlab.com/aulia-adil/proctor-room-selenium-test

# Run selenium docker-nya
sudo docker run --name peserta_ujian -p 8588:8588 \
-d -i --net host test_pj:v4 python3 /root/proctor-room-selenium-test/peserta_ujian.py