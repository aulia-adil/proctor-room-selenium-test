FROM registry.gitlab.com/aulia-adil/kp-master-process/selenium_docker:latest

COPY ./peserta_ujian.py /root/

CMD ["python3", "/root/peserta_ujian.py"]
